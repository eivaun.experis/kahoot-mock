# Quiz
A simple quiz app made as an exercise in Qt.

## Build
The project is built with Qt/qmake. Use Qt Creator to compile the application.

## Features
* Read quizes from json files.
* Varying amount of answers per question.
* Timed questions

## Contributors
Eivind Vold Aunebakk

## Screenshots
The start screen
![start](screenshots/start.png)
Select quiz
![select](screenshots/select.png)
A question
![question](screenshots/question.png)
Correct answer
![correct answer](screenshots/correct.png)
Timed question
![timed question](screenshots/time.png)
Results screen
![results](screenshots/results.png)
