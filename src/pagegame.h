#pragma once

#include "stackedpage.h"
#include "ui_mainwindow.h"
#include "gamestate.h"

#include <QTime>
#include <QTimer>
#include <QParallelAnimationGroup>

class PageGame : public StackedPage<Ui::MainWindow, GameState>
{
    Q_OBJECT
    Q_PROPERTY(QColor timeBarColor MEMBER timeBarColor WRITE setTimeBarColor)
public:
    PageGame(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);

    // StackedPage interface
    void onActivated() override;

    void nextQuestion();
    void endGame();
    void setScore(int score);

    void setTimeBarColor (QColor color);
private:
    void onAnswerButtonClicked(int answer);
    void onTimerUpdate();

    const QuizQuestion* currentQuestion;
    QTimer timer;
    QFont buttonFont;
    QParallelAnimationGroup *animation;
    QColor timeBarColor;
    QString timeBarStyle = QString("QProgressBar::chunk{ background-color: %1; }");
    QString answerButtonStyle = QString("QPushButton{ background-color: %1; border-color: %1; }");
    QStringList answerButtonColors = {"#FF6D6A", "#8BD3E6", "#77dd77", "#E9EC6B", "#EFBE7D", "#B1A2CA"};
};

