#include "pageresults.h"
#include "pagegame.h"
#include "pageselect.h"

PageResults::PageResults(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget) : StackedPage(sharedState, ui, parent, widget)
{
    connect(ui->results_button_again, &QPushButton::clicked, this, &PageResults::onButtonAgainClicked);
    connect(ui->results_button_exit, &QPushButton::clicked, this, &PageResults::onButtonExitClicked);
}

void PageResults::onActivated()
{
    auto result = getSharedState()->currentQuiz->getResult();
    getSharedState()->currentQuiz->reset();

    ui->results_score->setText(QString::number(result.score));
    ui->results_correct->setText(QString("%1/%2 correct answers").arg(result.numCorrect).arg(getSharedState()->currentQuiz->numQuestions()));
}

void PageResults::onButtonAgainClicked()
{
    StackedPage::setPage<PageGame>();
}

void PageResults::onButtonExitClicked()
{
    StackedPage::setPage<PageSelect>();
}
