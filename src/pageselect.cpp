#include "pageselect.h"

#include <QDir>
#include <QFileDialog>
#include "pagegame.h"

QString defaultPath = QDir::currentPath() + QDir::toNativeSeparators("/quiz");

PageSelect::PageSelect(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget) : StackedPage(sharedState, ui, parent, widget)
{
    connect(ui->select_quiz_table, &QTableWidget::cellClicked, this, &PageSelect::onTablCellClicked);
    connect(ui->select_quiz_table, &QTableWidget::cellDoubleClicked, this, &PageSelect::onTableCellDoubleClicked);

    connect(ui->actionOpen_quiz_folder, &QAction::triggered, this, &PageSelect::onOpenFolder);
    connect(ui->select_start_button, &QPushButton::clicked, this, &PageSelect::onStartButtonClicked);
    connect(&watcher, &QFileSystemWatcher::directoryChanged, this, &PageSelect::onDirectoryChanged);
}

void PageSelect::onActivated()
{
    ui->select_quiz_table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch );
    ui->menubar->show();
    if(quizDir.isEmpty())
    {
        setQuizFolder(defaultPath);
    }

    ui->statusbar->addWidget(&dirStatusWidget);
    dirStatusWidget.show();
    ui->statusbar->show();
}

void PageSelect::deActivated()
{
    ui->menubar->hide();
    ui->statusbar->hide();
    ui->statusbar->removeWidget(&dirStatusWidget);
}

void PageSelect::setQuizFolder(QString path)
{
    if(!quizDir.isEmpty()) { watcher.removePath(quizDir); }
    if(!path.isEmpty()) { watcher.addPath(path); }

    quizDir = path;
    dirStatusWidget.setText(quizDir);

    updateQuizes();
}

void PageSelect::updateQuizes()
{
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);

    QDir dir(quizDir);
    QStringList filters;
    filters << "*.json";
    dir.setNameFilters(filters);
    auto entries = dir.entryInfoList();

    quizes.clear();
    ui->select_quiz_table->clearContents();
    ui->select_quiz_table->setRowCount(0);
    for(qsizetype i = 0; i < entries.size(); i++)
    {
        Quiz q;
        if(q.loadFromFile(entries[i].absoluteFilePath()))
        {
            ui->select_quiz_table->insertRow(i);
            ui->select_quiz_table->setItem(i, 0, new QTableWidgetItem(q.getTitle()));
            ui->select_quiz_table->setItem(i, 1, new QTableWidgetItem(QString::number(q.numQuestions())));
        }
        quizes.push_back(q);
    }

    onTablCellClicked(0, 0);

    QGuiApplication::restoreOverrideCursor();
}

void PageSelect::onOpenFolder()
{
    QString dir = QFileDialog::getExistingDirectory(ui->centralwidget, "Open quiz folder", quizDir);
    if(!dir.isEmpty())
        setQuizFolder(dir);
}

void PageSelect::onTablCellClicked(int row, int)
{
    if(static_cast<size_t>(row) >= quizes.size())
    {
        getSharedState()->currentQuiz = nullptr;
        ui->select_quiz_name->setText("");
        ui->select_question_list->clear();
        ui->select_start_button->setEnabled(false);
        return;
    }

    getSharedState()->currentQuiz = &quizes[row];

    ui->select_quiz_name->setText(getSharedState()->currentQuiz->getTitle());

    ui->select_question_list->clear();
    for(const auto& question : getSharedState()->currentQuiz->getQuestions())
    {
        ui->select_question_list->addItem(question.title);
    }
    ui->select_start_button->setEnabled(true);
}

void PageSelect::onTableCellDoubleClicked(int row, int column)
{
    onTablCellClicked(row, column);
    onStartButtonClicked();
}

void PageSelect::onStartButtonClicked()
{
    if(getSharedState()->currentQuiz != nullptr)
        StackedPage::setPage<PageGame>();
}

void PageSelect::onDirectoryChanged(const QString&)
{
    updateQuizes();
}
