#include "pagegame.h"
#include "pageanswer.h"

PageAnswer::PageAnswer(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget) : StackedPage(sharedState, ui, parent, widget)
{
    connect(ui->answer_button_continue, &QPushButton::clicked, this, &PageAnswer::onContinueButtonClicked);
}

void PageAnswer::onActivated()
{
    ui->answer_score->setText(QString("%1 points").arg(getSharedState()->pointsGained));

    QString text = getSharedState()->pointsGained > 0 ? "Correct" : "Wrong";
    ui->answer_correct->setText(text);
}

void PageAnswer::onContinueButtonClicked()
{
    StackedPage::setPage<PageGame>();
}
