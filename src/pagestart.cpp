#include "pagestart.h"
#include "pageselect.h"

PageStart::PageStart(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget) : StackedPage(sharedState, ui, parent, widget)
{
    connect(ui->start_button, &QPushButton::clicked, this, &PageStart::onStartButtonClicked);
    connect(ui->start_name_input, &QLineEdit::returnPressed, this, &PageStart::onNameInputReturnPressed);
    connect(ui->start_name_input, &QLineEdit::textChanged, this, &PageStart::onNameInputTextChanged);
}

void PageStart::onActivated()
{
    ui->start_name_input->clear();
    ui->page_start->setFocus();
    onNameInputTextChanged(ui->start_name_input->text());
}

void PageStart::onStartButtonClicked()
{
    if(!ui->start_name_input->text().isEmpty())
    {
        getSharedState()->playerName = ui->start_name_input->text();
        StackedPage::setPage<PageSelect>();
    }
}

void PageStart::onNameInputReturnPressed()
{
    onStartButtonClicked();
}

void PageStart::onNameInputTextChanged(const QString &text)
{
    ui->start_button->setEnabled(!text.isEmpty());

}
