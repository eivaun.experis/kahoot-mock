#pragma once

#include <QString>
#include "quiz.h"

// The global game state shared between the pages.
struct GameState
{
    QString playerName;
    Quiz* currentQuiz;

    // The amount of points gainded from the last answer.
    int pointsGained;
};
