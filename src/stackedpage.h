#pragma once

#include <QObject>
#include <QWidget>
#include <QStackedWidget>
#include <vector>
#include <initializer_list>

// Designed to contain the logic for a single page in a QStackedWidget.
template<class TUI, class TSharedState>
class StackedPage : public QObject
{
public:
    using UiType = TUI;
    using SharedStateType = TSharedState;

    StackedPage(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);
    virtual ~StackedPage() {}

    // Sets this as the active page.
    void setActive();

    // Gets the shared state object.
    SharedStateType* getSharedState() { return sharedState; }

    // Called when the page is set as active.
    virtual void onActivated() {}

    // Called when the page is set as inactive.
    virtual void deActivated() {}

    // Adds pages that can be set with setPage.
    static void addPages(std::initializer_list<StackedPage*> pages);

    // Sets the active page.
    static void setPage(size_t index);

    // Sets the active page.
    template<class PageType>
    static void setPage();

protected:
    UiType* ui;

private:
    QWidget* widget = nullptr;
    QStackedWidget* parent = nullptr;
    SharedStateType* sharedState;

    inline static StackedPage* s_Current = nullptr;
    inline static std::vector<StackedPage*> s_Pages;
};

template<class TUI, class TSharedState>
StackedPage<TUI, TSharedState>::StackedPage(SharedStateType *sharedState, UiType *ui, QStackedWidget *parent, QWidget *widget)
{
    this->sharedState = sharedState;
    this->ui = ui;
    this->parent = parent;
    this->widget = widget;
}

template<class TUI, class TSharedState>
void StackedPage<TUI, TSharedState>::setActive()
{
    if(s_Current != nullptr)
        s_Current->deActivated();
    s_Current = this;
    parent->setCurrentWidget(widget);
    onActivated();
}

template<class TUI, class TSharedState>
void StackedPage<TUI, TSharedState>::addPages(std::initializer_list<StackedPage *> pages)
{
    for(auto p : pages)
        s_Pages.push_back(p);
}

template<class TUI, class TSharedState>
void StackedPage<TUI, TSharedState>::setPage(size_t index)
{
    s_Pages[index]->setActive();
}

template<class TUI, class TSharedState>
template<class PageType>
void StackedPage<TUI, TSharedState>::setPage()
{
    for(auto p : s_Pages)
    {
        if(dynamic_cast<PageType*>(p))
        {
            p->setActive();
            return;
        }
    }
    qCritical("Missing page");
}
