#include "quiz.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

const QString &Quiz::getTitle() const
{
    return title;
}

const QuizQuestion *Quiz::nextQuestion()
{
    if(questions.size() > current_question)
    {
        return &questions[current_question++];
    }
    return nullptr;
}

int Quiz::answerQuestion(int answer, int timeLeft)
{
    if(answer == questions[current_question-1].correctAnswer)
    {
        result.numCorrect++;

        float scoreMul = 1.0f;
        if(timeLeft > 0)
            scoreMul = (float)timeLeft / (float)questions[current_question-1].time;
        int score = 1000 * scoreMul;
        result.score += score;

        return score;
    }
    return 0;
}

const QuizResult &Quiz::getResult() const
{
    return result;
}

size_t Quiz::numQuestions() const
{
    return questions.size();
}

const std::vector<QuizQuestion> &Quiz::getQuestions() const
{
    return questions;
}

void Quiz::reset()
{
    current_question = 0;
    result.score = 0;
    result.numCorrect = 0;
}

bool Quiz::loadFromFile(QString path)
{
    reset();

    QString val;
    QFile file;
    file.setFileName(path);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        val = file.readAll();
        file.close();
    }
    else
    {
        return false;
    }

    QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject jRoot = d.object();

    title = jRoot.value(QString("title")).toString();
    QJsonArray jQuestions = jRoot.value(QString("questions")).toArray();

    questions.clear();
    for (const auto &jQuestion : jQuestions)
    {
        const auto &qObj = jQuestion.toObject();

        QuizQuestion question;
        question.title = qObj.value(QString("question")).toString();
        question.correctAnswer = qObj.value(QString("correct_answer")).toInteger();

        if(qObj.contains("time"))
            question.time = qObj.value(QString("time")).toInteger();
        else
            question.time = 0;

        QJsonArray jAnswers = qObj.value(QString("answers")).toArray();
        for (const auto &jAnswer : jAnswers)
        {
            question.answers.push_back(jAnswer.toString());
        }
        questions.push_back(question);
    }

    return true;
}
