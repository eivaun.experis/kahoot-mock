#pragma once

#include "stackedpage.h"
#include "ui_mainwindow.h"
#include "quiz.h"
#include "gamestate.h"

class PageResults : public StackedPage<Ui::MainWindow, GameState>
{
public:
    PageResults(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);

    // StackedPage interface
    void onActivated() override;

private:
    void onButtonAgainClicked();
    void onButtonExitClicked();
};
