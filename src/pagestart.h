#pragma once

#include "stackedpage.h"
#include "ui_mainwindow.h"
#include "gamestate.h"

class PageStart : public StackedPage<Ui::MainWindow, GameState>
{
public:
    PageStart(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);

    // StackedPage interface
    void onActivated() override;

private:
    void onStartButtonClicked();
    void onNameInputReturnPressed();
    void onNameInputTextChanged(const QString &text);
};
