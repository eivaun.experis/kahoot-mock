#pragma once

#include <QFileSystemWatcher>

#include "stackedpage.h"
#include "ui_mainwindow.h"
#include "gamestate.h"
#include "quiz.h"

class PageSelect : public StackedPage<Ui::MainWindow, GameState>
{
public:
    PageSelect(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);

    // StackedPage interface
    void onActivated() override;
    void deActivated() override;

    void setQuizFolder(QString path);
    void updateQuizes();

private:
    void onOpenFolder();
    void onTablCellClicked(int row, int);
    void onTableCellDoubleClicked(int row, int column);
    void onStartButtonClicked();
    void onDirectoryChanged(const QString& str);

    std::vector<Quiz> quizes;
    QString quizDir;
    QLabel dirStatusWidget;
    QFileSystemWatcher watcher;
};
