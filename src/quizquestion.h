#pragma once

#include <vector>
#include <QString>

struct QuizQuestion
{
    QString title;
    int correctAnswer;
    int time;
    std::vector<QString> answers;
};
