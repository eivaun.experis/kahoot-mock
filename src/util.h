#pragma once

#include <QLayout>
#include <QWidget>
#include <QColor>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <iostream>
#include <QMetaProperty>

// Removes all items from a QLayout.
inline void clearLayout(QLayout *layout, bool deleteWidgets = true)
{
    while (QLayoutItem *item = layout->takeAt(0))
    {
        if (deleteWidgets)
        {
            if (QWidget *widget = item->widget())
                widget->deleteLater();
        }
        if (QLayout *childLayout = item->layout())
            clearLayout(childLayout, deleteWidgets);
        delete item;
    }
}

// Linear interpolation between two colors.
inline QColor interpolateColor(QColor color1, QColor color2, float fraction)
{
    return QColor(
        color1.red()* (1-fraction) + color2.red()*fraction,
        color1.green()* (1-fraction) + color2.green()*fraction,
        color1.blue()* (1-fraction) + color2.blue()*fraction,
        255);
}

inline void setAnimGroupDuration(QParallelAnimationGroup& group, int duration)
{
    for (int i = 0; i < group.animationCount(); i++)
    {
        if(auto qPpropAnim = dynamic_cast<QPropertyAnimation*>(group.animationAt(i)))
            qPpropAnim->setDuration(duration);
    }
}


inline void listProperties(const QObject* obj)
{
    std::cout << "\nPROPERTIES\n";
    const QMetaObject *metaobject = obj->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        std::cout << name << "\n";
    }
    std::cout << "END PROPERTIES\n" << std::endl;
}
