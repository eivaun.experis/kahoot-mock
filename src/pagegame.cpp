#include "pagegame.h"

#include "util.h"
#include "pageanswer.h"
#include "pageresults.h"
#include <QPropertyAnimation>

PageGame::PageGame(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget) : StackedPage(sharedState, ui, parent, widget), timer(this)
{
    connect(&timer, &QTimer::timeout, this, &PageGame::onTimerUpdate);
    buttonFont = ui->game_answer_0->font();

    animation = new QParallelAnimationGroup(this);
    animation->setCurrentTime(0);

    auto valueAnim = new QPropertyAnimation(ui->game_time_bar, "value");
    valueAnim->setStartValue(100);
    valueAnim->setEndValue(0);
    animation->addAnimation(valueAnim);

    auto colorAnim = new QPropertyAnimation(this, "timeBarColor");
    colorAnim->setStartValue(QColor(0, 255, 0));
    colorAnim->setEndValue(QColor(255, 0, 0));
    animation->addAnimation(colorAnim);
}

void PageGame::onActivated()
{
    ui->game_player_name->setText(getSharedState()->playerName);
    ui->game_quiz_title->setText(getSharedState()->currentQuiz->getTitle());
    setScore(getSharedState()->currentQuiz->getResult().score);

    nextQuestion();
}

void PageGame::nextQuestion()
{
    currentQuestion = getSharedState()->currentQuiz->nextQuestion();
    if(!currentQuestion)
    {
        endGame();
        return;
    }

    clearLayout(ui->game_answers, true);
    ui->game_question_title->setText(currentQuestion->title);

    for (size_t i = 0; i < currentQuestion->answers.size(); i++)
    {
        QPushButton *b = new QPushButton(currentQuestion->answers[i]);
        b->setFont(buttonFont);
        b->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        b->setStyleSheet(answerButtonStyle.arg(answerButtonColors[i%6]));
        ui->game_answers->addWidget(b);
        b->show();
        connect(b, &QPushButton::clicked, this, [this, i](){
            onAnswerButtonClicked(i);
        });
    }

    if(currentQuestion->time > 0)
    {
        setAnimGroupDuration(*animation, currentQuestion->time);
        animation->start();
        timer.start(currentQuestion->time);
        ui->game_time_bar->show();
    }
    else
    {
        ui->game_time_bar->hide();
    }
}

void PageGame::endGame()
{
    StackedPage::setPage<PageResults>();
}

void PageGame::setScore(int score)
{
    ui->game_player_score->setText(QString::number(score));
}

void PageGame::setTimeBarColor(QColor color)
{
    timeBarColor = color;
    ui->game_time_bar->setStyleSheet(timeBarStyle.arg(color.name()));
}

void PageGame::onAnswerButtonClicked(int answer)
{
    timer.stop();
    animation->stop();

    if(currentQuestion == nullptr) { return; }

    int timeLeft = 0;
    if(currentQuestion->time > 0)
        timeLeft = animation->totalDuration() - animation->currentTime();

    getSharedState()->pointsGained = getSharedState()->currentQuiz->answerQuestion(answer, timeLeft);
    StackedPage::setPage<PageAnswer>();
}

void PageGame::onTimerUpdate()
{
    onAnswerButtonClicked(-1);
}
