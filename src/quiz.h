#pragma once

#include <vector>
#include <QString>

#include "quizquestion.h"

struct QuizResult
{
    int score = 0;
    int numCorrect = 0;
};

class Quiz
{
    QString title;
    size_t current_question = 0;
    std::vector<QuizQuestion> questions;

    QuizResult result;

public:
    Quiz() = default;

    // Get the title of the quiz.
    const QString& getTitle() const;

    // Get the next question in the quiz.
    const QuizQuestion* nextQuestion();

    // Submit an answer to the current question. Returns the amout of points gained.
    int answerQuestion(int answer, int timeLeft);

    // Get the result of the quiz.
    const QuizResult& getResult() const;

    // Get the number of questions in the quiz.
    size_t numQuestions() const;

    // Get all the questions in the quiz.
    const std::vector<QuizQuestion>& getQuestions() const;

    // Resets the quiz. Clears the results and the next call to nextQuestion will return the first question.
    void reset();

    // Loads the quiz from a json file.
    bool loadFromFile(QString path); 
};
