#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "pagestart.h"
#include "pageselect.h"
#include "pagegame.h"
#include "pageresults.h"
#include "pageanswer.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    instance = this;

    ui->setupUi(this);
    ui->menubar->hide();
    ui->statusbar->hide();

    startPage = std::make_unique<PageStart>(&gameState, ui, ui->stackedWidget, ui->page_start);
    selectPage = std::make_unique<PageSelect>(&gameState, ui, ui->stackedWidget, ui->page_select);
    gamePage = std::make_unique<PageGame>(&gameState, ui, ui->stackedWidget, ui->page_game);
    resultsPage = std::make_unique<PageResults>(&gameState, ui, ui->stackedWidget, ui->page_restults);
    answerPage = std::make_unique<PageAnswer>(&gameState, ui, ui->stackedWidget, ui->page_answer);

    StackedPage<Ui::MainWindow, GameState>::addPages({startPage.get(), selectPage.get(), gamePage.get(), resultsPage.get(), answerPage.get()});

    startPage->setActive();
}

MainWindow::~MainWindow()
{
    delete ui;
}
