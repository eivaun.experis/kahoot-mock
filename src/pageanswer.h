#pragma once

#include "stackedpage.h"
#include "ui_mainwindow.h"
#include "gamestate.h"

class PageAnswer : public StackedPage<Ui::MainWindow, GameState>
{
public:
    PageAnswer(SharedStateType* sharedState, UiType* ui,  QStackedWidget* parent, QWidget* widget);

    // StackedPage interface
    void onActivated() override;

private:
    void onContinueButtonClicked();
};

