#pragma once

#include <memory>

#include <QLabel>
#include <QMainWindow>

#include "stackedpage.h"
#include "gamestate.h"

QT_BEGIN_NAMESPACE
namespace Ui{ class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    static MainWindow* Get() { return instance; }

private:
    inline static MainWindow* instance = nullptr;

    Ui::MainWindow *ui;
    GameState gameState;
    std::unique_ptr<StackedPage<Ui::MainWindow, GameState>> selectPage, startPage, gamePage, resultsPage, answerPage;
};
